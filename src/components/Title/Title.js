import React from 'react';
import topLayer from './images/title_top_layer.png';
import titleBottomLayer from './images/title_bottom_layer.png';
import titleLogoLayer from './images/title_logo_layer.png';
import titleLogo from './images/title_logo.png';
import sstLogo from './images/smart_stax_logo.png';
import vtdLogo from './images/vt_doublepro_logo.png';
import dghLogo from './images/droughtgard_logo.png';
import button from './images/button_img.png';
import Planting from '../Planting/Planting';

class Title extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            route: 'home'
        }
    }

    startButton = () => {

    };

    render() {
        const {styles} = this.props;
        const theText = 'See how our technology protects your seed investment';
        return (
            <div>
                {/* Header Area */}
                <div className={styles.titleTop} style={{backgroundImage: `url(${topLayer})`}}>
                </div>

                {/* Logo Area */}
                <div className={styles.titleLogo} style={{backgroundImage: `url(${titleLogoLayer})`}}>
                    <img src={titleLogo} alt=""/>
                </div>

                {/* Footer Area */}
                <div className={styles.titleBottom} style={{backgroundImage: `url(${titleBottomLayer})`}}>
                    {/* Container */}
                    <div style={{padding: '40px'}}>
                        {/* Logos Area */}
                        <div className={styles.titleLogos}>
                            <ul>
                                <li><img src={sstLogo} alt="SmartStax Technology Logo"/></li>
                                <li><img src={vtdLogo} alt="VTDouble PRO Technology Logo"/></li>
                                <li><img src={dghLogo} style={{paddingTop: '15px'}} alt="DroughtGard Hybrids Logo"/></li>
                            </ul>
                        </div>
                        {/* Title Text Area
                            These text will change base what what page the user is on
                        */}
                        <hr/>
                        <div className={styles.titleText}>
                            <span>{theText}</span>
                        </div>
                        <hr/>
                        {/* Start Button */}
                        <div
                            className={styles.btn}
                            style={{backgroundImage: `url(${button})`}}
                            onClick={this.startButton}
                        >
                            <span>Start</span>
                        </div>

                    </div>
                </div>



                {/* change route state to ty page if user finished viewing the last page*/}
            </div>
        );
    }
}

export default Title;


