import React from 'react';
const Navigation = ({styles}) => {
    return (
        <div className={styles.nav}>
            <ul>
                <li><a href="#">Planting</a></li>
                <li><a href="#">Growing</a></li>
                <li><a href="#">Harvest</a></li>
            </ul>
        </div>
    );
};

export default Navigation;
