import React from 'react';
import logo from './images/logo.png';
import imgUrl from './images/header-bg.png';
const Header = ({styles}) => {
    return (
        <div className={styles.header}>
            <div className={styles.headerBg}>
                <img src={imgUrl} alt=""/>
            </div>
            <img src={logo} alt="Traits Experience Logo" className={styles.logo}/>

        </div>
    );
};

export default Header;

{/*<div className={styles.header} style={{backgroundImage: `url(${imgUrl}`}}>*/}