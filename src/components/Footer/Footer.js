import React from 'react';
import imgUrl from './images/footer-bg.png';
const Header = ({styles}) => {
    return (
        <div className={styles.footer}>
            <img src={imgUrl} alt=""/>
        </div>
    );
};

export default Header;