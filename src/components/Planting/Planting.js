import React from 'react';
import Header from '../Header/Header';
import Navigation from '../Navigation/Navigation';
import Footer from '../Footer/Footer';
const Planting = ({styles}) => {
    return (
        <div>
          <Header styles={styles}/>
            <Navigation styles={styles} />
            <Footer styles={styles} />
        </div>
    );
};

export default Planting;