import React, { Component } from 'react';
import Header from './components/Header/Header';
import Navigation from './components/Navigation/Navigation';
import Footer from './components/Footer/Footer';
import { Route, Link, BrowserRouter as Router} from 'react-router-dom';
import styles from './App.scss';

/* Images */
import topLayer from './images/title_top_layer.png';
import titleBottomLayer from './images/title_bottom_layer.png';
import titleLogoLayer from './images/title_logo_layer.png';
import titleLogo from './images/title_logo.png';
import sstLogo from './images/smart_stax_logo.png';
import vtdLogo from './images/vt_doublepro_logo.png';
import dghLogo from './images/droughtgard_logo.png';
import button from './images/button_img.png';

class Title extends React.Component {
    constructor() {
        super();
        this.state = {
            route: 'title',
            theText: 'See how our technology protects your seed investment',
            isActive: true
        }
    }

    render() {
        return (
            <div>
                {/* Header Area */}
                <div className={styles.titleTop} style={{backgroundImage: `url(${topLayer})`}}>
                </div>

                {/* Logo Area */}
                <div className={styles.titleLogo} style={{backgroundImage: `url(${titleLogoLayer})`}}>
                    <img src={titleLogo} alt=""/>
                </div>

                {/* Footer Area */}
                <div className={styles.titleBottom} style={{backgroundImage: `url(${titleBottomLayer})`}}>
                    {/* Container */}
                    <div style={{padding: '40px'}}>
                        {/* Logos Area */}
                        <div className={styles.titleLogos}>
                            <ul>
                                <li><img src={sstLogo} alt="SmartStax Technology Logo"/></li>
                                <li><img src={vtdLogo} alt="VTDouble PRO Technology Logo"/></li>
                                <li><img src={dghLogo} style={{paddingTop: '15px'}} alt="DroughtGard Hybrids Logo"/></li>
                            </ul>
                        </div>
                        {/* Title Text Area
                         These text will change base what what page the user is on
                         */}
                        <hr/>
                        <div className={styles.titleText}>
                            <span>{this.state.theText}</span>
                        </div>
                        <hr/>
                        {/* Start Button */}
                        <div
                            className={styles.btn}
                            style={{backgroundImage: `url(${button})`}}
                        >
                            <span><Link to='/planting'>Start</Link></span>
                        </div>

                    </div>
                </div>



                {/* change route state to ty page if user finished viewing the last page*/}
            </div>
        );
    }

}

const Planting = () => {
    return (
        <div>
            <Header styles={styles}/>
            <Navigation styles={styles} />
            <Footer styles={styles} />
        </div>
    );
};

class App extends Component {
    render() {
        return (
            <Router>
              <div className={styles.app}>
                  <Route exact path='/' component={Title} />
                  <Route path='/planting' component={Planting} />
              </div>
            </Router>
        );
    }
}

export default App;
